console.log("Hello World");



let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasur"],
	friends: {
		hoenn : ["May", "Max"],
		kanto : ["Brock", "Misty"]
	}
}
console.log(trainer);
// Initialize/add the given object properties and methods
console.log("Result of dot notation");
console.log (trainer.name);
console.log ("Result of square bracket notation");
console.log (trainer.pokemon);
console.log ("Result of talk method");
console.log (trainer.pokemon[0] + " ! I choose you"); 

// Properties
	function Pokemon(name,level,health,attack) {
		this.name = name;
		this.level = level;
		this.health = health;
		this.attack = attack;
		this.tackle = function(target){
			target.health = (target.health - this.attack);
			console.log(this.name + ' tackled ' + target.name);
			console.log(target.name +'`s health is reduced to '+ target.health);
			this.faint = function(target){
				if (target.health <= 0)
					console.log(target.name+ ' fainted');
			} 
		};
		
		}
	Pokemon();

	let newPokemonA = new Pokemon(trainer.pokemon[0],12,24,12);
	
	console.log (newPokemonA);

	let newPokemonB = new Pokemon("Geodude",8,16,8);
	
	console.log(newPokemonB);

	let newPokemonC = new Pokemon("Mewtwo",100,200,100);
	
	console.log(newPokemonC);


	newPokemonB.tackle(newPokemonA);
	console.log(newPokemonA);

	newPokemonC.tackle(newPokemonB);
	newPokemonC.faint(newPokemonB);
	console.log(newPokemonB);

	
	//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
	



	

