// the "User" variable is defined using a capitalized letter yo indicate that what we are using is the "USer" model code readability
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
// npm install bcrypt- encrypting password 
const auth = require("../auth");



// Check if email already exist
/*
	Steps:
	1. USe mongoose "find" method to duplicate emails
	2. USe the "then" method to send a respond back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the frontend via the "then" method found in the route file 
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};

// User Registration
/*
	Steps:
	1/Create a new User object using the mongoose model and the information request body
	2. MAke sure that the password is encryptrd
	3.Sve the ne User to the databse
*/

module.exports.registerUser = (reqBody) =>{
	
	// Creates a variable "newUser" andinstantiates a new "User" object using the mongoose model 
	// Uses the information from the body provide all the necessart information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)

	})


	// Saves the created object to our databse
	return newUser.save().then((user, error)  =>{
		// User registration failed
		if(error) {
			return false;
		} else {
			
			return true;
		};
	})
}


// User authentication

/*
	Steps:
	1.Check the database if the user email exist
	2, Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the same criteria
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of the comparing login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login from to the encrypted password retrieved from the database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled
				return {access : auth.createAccessToken(result)}
			// Passwords do not match
			} else {
				return false;
			}
		}
	})
}



// S38 ACTIVITY

// Retrieve user details
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Reassign the password of the returned document to an empty string
			3. Return the result back to the frontend
		*/
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		if (result == null){
			return false
		}else {
			result.password = "";
		}
		

		// Returns the user information with the password as an empty string
		return result;

	});

};


// Enroll user to a class

/*
	Sreps:
	1.Fnd the document n the databasa using the user's ID
	2. Add the course ID to the user's enrollment array
	3.Update the document in the MongoDB Atlas Database
*/

// Async await will be used in enrolling the user because we will need to update 2 seperate documents when enrollung a user



module.exports.enroll = async(data)=>{
	// Add the courseID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns upon successful update of otherwise false
	// Using the

	let isUserUpdated = await User.findById(data.userId).then(user=>{

		return user.save().then((user,error)=>{
			if (error){
				return false
			}else{
				return true
			}
		})
	})
	 // Add the userID in the enrolles array of the course
	 // Using the 'await' keyword will allow the enroll method to complete updating the course before returning a response back to the front end // 

	 let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
	 	// Adds the userId in the course's enrollees array
	 	course.enrollees.push({userId: data.userId})
// Saves the updated course information in the database
	 	return course.save().then((course,error)=>{
	 		if(error){
	 			return false
	 		}else{
	 			return true
	 		}
	 	})
	 })

	 // Condition that willl check if the user and Course documents have been updated
	 // User enrollment successful

	 if(isUserUpdated && isCourseUpdated){
	 	return true

	 	// user enrollment failure
	 }else{
	 	return false
	 }
};

