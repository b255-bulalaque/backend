// Everthing here consist of SCHEMA(blueprint of data)

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type: String,
		// Requires the data for our field/properties to be included when crating a record
		// the "true" value defines if the field is required or not and the second element in the arrat is he message that will be printed out in our terminal when the data is not present
		required : [true, "Course is required"]
	},

	description : {
		type : String,
		required : [true, "Description is required"]
	},

	// default means it will  be automatically true
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		// The "new Date()" expression isntantiates a new "date" that stores the current date and time whenever a course is created in our database 
		default: new Date()
	},

	// The "enrollees" property/field will be an array of objects containing the user ID's and date and time that user enrolled to the course
	// Will be applying the concept of referencing data to establish a relationship between our courses and users
	enrollees : [
	{
		userId : {
			type: String,
			required: [true, "UserId is required"]
		},
		enrolledOn : {
			type: Date,
			default: new Date()
		}
	}]
})


module.exports = mongoose.model("Course", courseSchema);