console.log('Hello World');

// Array Methods

// JavaSCript has built-in functions and methods for rrays. This allows us to manipulate and access arrray itmes.

// Mutator Methods

/*
	-Mutator Methods are functions that "Mutate" or change an array after they're created
	-Tese methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ['Apple', 'Orange','Kiwi', 'Dragon Fruit'];

// push	
/*
	-Adds an elements in the end of an array AND returns the array's length
*/

console.log('Current array: ');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);  
// returns the array's length
console.log('Mutated array from push method:');
console.log(fruits);


// adding multiple elements to an array

fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);

// pop()
/*
	-Removes the last elements in an array AND returns the removed elements
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);

// fruits.pop();
// console.log(fruits);


// unshift
/*
	-Add an element at the beginning of an array
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);


// Shift()
/*
	-Removes an elements at the beginning of an array AND returns the removed elemets
*/

let anotherFruit = fruits.shift()
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice
/*
	Simultaneously removes elemets from specified index number and adds elemets
	(delete and insert)
*/

fruits.splice(1,2,'Lime','Cherry');
console.log('Mutated array from splice method:');
console.log(fruits);

// sort
/*
	-Rearranges the array elements in alphanumeric order
	-arrange numerical then alphabet
	-disadvantage ng sort() method is yung number nassort nya kapag yung 1st digit lang hindi sa laki ng digit ex. 1, 2, 2093, 
*/

fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);

/*
	Important Note:
	-The "sort" method is used for more complpicated sorting functions
*/

// reverse()

/*
	-Reverses the order of array elements
*/

fruits.reverse()
console.log('Mutated array from reverse method:');
console.log(fruits);

// Non-Mutator Methods
/*
	-Non mutators methods are functions that do not modify or chenge an array after thet're created
	-These methos do not manipulate original array performing various task such as returning elements from an array and combining arrays
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH','FR','DE'];


// indexOf()
/*
	-Returns the index number of the first matching elements found in an array
	-If no match was founs, the result will be -1
	-The search process will be done from first elemet proceeding to the last element
	-left to right
*/

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf()
/*
	-Returns the index number of the last matching element found in an array
	-The search process wil be done from the last element proceeding to the first element
	-CASE SENSITIVE
	-right to left
*/

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// 6- specification means  what index to start 
let lastIndexStart = countries.lastIndexOf('PH',3);
console.log('Result of lastIndexOf method: ' + lastIndexStart);

// slice
/*
	-Portions/Slice elements from an array AND returns a new array
*/

let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ' )
console.log(slicedArrayA);

/*di maaffect*/
console.log(countries);

let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method: ' )
console.log(slicedArrayB);


// started to the last mimus 3
let slicedArrayC = countries.slice(-3);
console.log('Result from slice method: ' )
console.log(slicedArrayC);


// tostring()
/*
	-returns an array as aa string seperated by comma
*/

let stringArray = countries.toString()
console.log('Result from toString method: ' )
console.log(stringArray);

// concat()
/*
	-Combines 2 arrays and returns the combine result
*/

let taskArrayA = ['drink html', 'eatJavaScript'];
let taskArrayB = ['inhale css', 'breath sass'];
let taskArrayC = ['get git', 'be node'];

let task = taskArrayA.concat(taskArrayB)
console.log('Result from concat method: ' )
console.log(task);

// combining multiple arrays- separate with comma (,)
console.log('Result from concat method: ' )
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);


// Combining arrays with elements
let combinedtasks = taskArrayA.concat('smell express', 'throw react');
console.log('Result from concat method: ' )
console.log(combinedtasks);




// join()
/*
	-returns an array as a string separated by specified separator string
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join(' s'));
console.log(users.join(' - '));


// Iteration Methods
/*
	- Iteration methods are loops designed to perform repititive task on arrays
	-Iteration methods loops over all items in an array 
	-Useful for manipulation array data resulting in complex tasks
	-Array iteration methods normally work with afunction supplied as an arguments
	-How these function work is by performing tasks that are pre-defined within an array's method
*/

// forEach()
/*
	-Similar to a for loop that iterates on each array element
	-for each item in the array, the anonymous function passed in the function is able to received thr current item being iterated or loop over by assigning a parameter
	-It's common practice to use the singular form of the array content for parameter names used in array loops
*/

allTasks.forEach(function(task) {
	console.log(task);
});

// Using forEach with conditional statement
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
});
console.log('Result of filtered Task: ')
console.log(filteredTasks);


// map()
/*
	-Iterates on each element AND return a new array with different value depending on the result of function operrion
	-this is useful for performing task where mutating/changing the elements are required
	-Unlike the forEach methof, the map method required the use of a "return" statement in order top creatwe another array with the performed operation
*/

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(numbers) {
	return numbers * numbers;
});

console.log('Original Array:');
console.log(numbers) 
// original array is unaffected by map()
console.log('Result of map method:')
console.log(numberMap);
// a new array is returned by map()

// map() vs forEach()
let numberForEach = numbers.forEach(function(numbers) {
	return numbers * numbers
})
console.log(numberForEach);

// forEach(), loops over all items in the array as does map() but forEach does not return a new array

// every()
/*
	-Checks if all the array meet the given condition
	-This is useful for validating data stored in arrays especially when dealing with large amounts of data
	-Returns a true value if all elements meet the condition and false if otherwise
*/

let allValid = numbers.every(function(numbers){
	return (numbers < 3);

});
console.log("Result of every method: ")
console.log(allValid);

// some()
/*
	-checks if ATLEAST ONe element in the array MEETs the given condition\
	-returns a true value if atleast one elements meets the condition and false if otherwise
*/

let someValid = numbers.some(function(numbers){
	return (numbers < 2);

});
console.log("Result of every method: ")
console.log(someValid);

// filter ()
/*
	-RETURNS NEW ARRAY THAT CONTAINS ELEMENTS WHICH MEETS THE GIVEN CONDITION
	-returns an empty array if no elements were found
	-useful for filtering array elemets with a given condition and shortens the syntax compared to using the othrt array iteration methods
	-Mastery of loops can help us work effectively by reducing the amount of code we use
	-nialalabs nya yung meet na condition
*/

let filterValid = numbers.filter(function(numbers){
	return (numbers < 3);

});
console.log("Result of every method: ")
console.log(filterValid);

// no lements found
let nothingFound = numbers.filter(function(numbers){
	return (numbers = 0);

});
console.log("Result of every method: ")
console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = []
;
numbers.forEach(function(numbers){
	if (numbers < 3){
		filteredNumbers.push(numbers);
		}
	});
console.log("Result of every method: ")
console.log(filteredNumbers);


// includes()
/*
	-includes() method checks if the argument passed can be found in the array
	-it returns a boolean which can be saved in a variable 
		-returns true if the arguments is found in the array
		-returns false if it is now
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

/*
	-Methods can be chained using them one afetr another
	-the result of the first method is used on the second method until all "chained" methods have been resolved
*/

let filteredProducts = products.filter(function(products){
	return products.toLowerCase().includes('a');
})
console.log(filteredProducts);

// reduce()
/*
	-evaulates elements from left to right and returns/reduces the array into a single value

*/



let iteration = 0
console.log(numbers);
let reducedArray = numbers.reduce(function(x,y){
	// used to track the current iteration count and accumalator/currentValue data
	console.warn('current iteration ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// the operation to reduce the array into a single value
	return x + y;
});

console.log('Result of reduce method:' + reducedArray);


// reducing string arrays
let list = ['Hello','Again','World'];

let reducedJoin = list.reduce(function(x,y){
	return x + ' ' + y
});
console.log('Result of reduce method: ' + reducedJoin);