// count

db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $count: "onSale"}
	]);

// count
db.fruits.aggregate([
		{ $match: {
			stock : {
				$gte :20
			}
		}},
		{ $count: "enoughStock"}
	]);


// average
db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id",
		
           avgPrice: { $avg: "$price" }

	}}
	]);


// max
db.fruits.aggregate([
		
		{ $group: { _id: "$supplier_id",
		
           maxPrice: { $max: "$price" }
	}}
	]);


// min
db.fruits.aggregate([
		
		{ $group: { _id: "$supplier_id",
		
           minPrice: { $min: "$price" }
	}}
	]);
