// Create Documents to use for our discussion

db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// [SECTION] MongoDB Aggregation
/*
	-used to generate manipulated data and perform operation to create filtered results that help in analyzing data
	-compared to doing CRUD operations on our data from the previous sessions, aggragation gives us access to manipulate, filter and compyte for results providing us with information to make necessary development decisions without having to create a fronted application
*/

// Using the agrregate method

// - the $ symbol will refer to a field name that is available in the documents that are being aggregated on

db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
	]);


// Field Projection with aggregation
/*
	$project can be used when aggregating data to include/exclude fields from the returned results
*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{ $group: { _id: "$supplier_id", total: { $sum: "$stock"}}},
		{$project: {_id: 0}}
	])


// Sorting aggregated results
/*
	-the $sort can be used to change the order of aggregated results
	-providing a value of -1 will sort the ggregated results in a reverse order
*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{ $group: { _id: "$supplier_id", total: { $sum: "$stock"}}},
		{$sort: {total: -1}}
	]);


// Aggregating results based on array fields

/*
	-the $unwind deconstruct an array fields from  collection/fields with an array value to output a result for each element
*/

db.fruits.aggregate([
		{$unwind: "$origin"}
	]);

// Display fruits decomuents by origin and the kinds of fruits that are supplied

db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group: {_id: "$origin", kinds: {$sum:1}}}
	]);

// [SECTION] Guidelines in schema design

/*
	-Schema design/ data modelling is an important feature when creting databases
	-MongoDB can be categorized into normalized anf de-normalized/embedded data
	-normalized data refer to a data structured when documents are referred to each other using their ID's for related pieces of information
	-De-normalized data/ embedded data returns to a data structure where related pieces of information is added to a documents as an embedded object
	-both dta structured are common practice but each of them have pros and cons
	-Normalized makes it easier to read information because seperate documents can be retrieve but in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documnets at the same time
	-This approach is recommended for data structures where pieces of information are commonly operated/chnaged
	-De- normalized data/embedded data makes it easier for query documents and ahs performance because only one query needs to be done in order to retrieve documents. However, if the data structure becomes too compex it makes it more difficult to manipulate and access information
*/

// One-to-one relationship
// Creates an id and stores it in the variable owner for use in documents creation

var owner = ObjectId()

db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "0987654321"
});

// Change the "owner_id" using the actual id in the previously created document
db.supplier.insert({
	name: "ABC fruits",
	contact: "1234567890",
	owner_id: owner
});


// One-To-Few Relationship
db.suppliers.insert({
  name: "DEF Fruits",
  contact: "1234567890",
  addresses : [
    { street: "123 San Jose St", city: "Manila"},
    { street: "367 Gil Puyat", city: "Makati"}
  ]
});

// One-To-Many Relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insert({
  _id: supplier,
  name: "GHI Fruits",
  contact: "1234567890",
  branches: [
    branch1
  ]
});

// Change the "<branch_id>" and "<supplier_id>" using the actual ids in the previously created document
db.branches.insert({
  _id: <branch_id>,
    name: "BF Homes",
    address: "123 Arcardio Santos St",
    city: "Paranaque",
    supplier_id: <supplier_id>
});

db.branches.insert(
  {
    _id: <branch_id>,
      name: "Rizal",
      address: "123 San Jose St",
      city: "Manila",
      supplier_id: <supplier_id>
  }
);
