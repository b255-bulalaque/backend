console.log("Hello World!");

// Finctions
	// Functions in JavaScript are linee/blocks of codes that tell our device/application to perform a certain task
// Functions are mostly created to create complicted task to run several lines of code in succession

// Function Declaration
	
		function printName() {
			console.log("My name is John");
		};

// Function Invocation

		printName();

// Function Decalaration versus Expressions

		// Function declaration 

		// A Function can be created trough function declaration by using the function keyword and adding a function name

		declaredFunction();

		function declaredFunction (){
			console.log("Hello World from declaredFunction");
		};

		declaredFunction();

		// FUnction Expression
		// A function can also be stored in a varaibale this is called function expression

		// A function expression is an anonymous function assigned to the variable to the variableFunction

		// Anonymous function is just a function without a name

		let variableFunction = function(){
			console.log("Hello Again");
		};

		variableFunction();

		// We can also created a function expression of a name function however to invike the function expression we invoke it by its vrible name not by it's function name

		let funcExpression = function funcName(){
			console.log("Hello from the other side");

		};

		funcExpression();

		 // You can reassign declared function and function expression to new anonymous funcrtions

		declaredFunction = function() {
			console.log("updated declaredFunction");
		};

		declaredFunction();

		funcExpression = function(){
			console.log("Updated funcExpression");
		};

		funcExpression();

		// However, we cannot reassign a function expression initialized with const
		/*
		const constantFunc + function(){
			console.log ("Initialized with const!")
		}

		constantFunc();

		constantFunc = function(){
			console.log("Cannot be assigned!")
		}

		constantFunc();*/

/*
	scopre is the accessibility (visibility) of variables within our program

	JavaScript Variables has 3 types of scopre
	1. local/block scopre
	2.global scope
	3. function scopre
*/

		{
			let localVar = "Armando Perez";
		}

		let globalVar = "Mr. Worldwide";

		console.log(globalVar);


		// Function Scope

		function showName(){
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}

		showName();


		// Nested Functions

			// You can created another funcrtion inside a function. this is called a nested function.

			function myNewFunction() {
				let name = "Jane";

				function nestedFunction (){
					let NestedName = "John";
					console.log(name);
				}
				// console.log(nestedName); ---if you console outside nested = error
				nestedFunction();
			}

			myNewFunction();


		// FUnction and Global Scopred Variables
			// Global scope variables
			let globalName = "Alexandro";

			function myNewFunction2() {
				let nameInside = "Renz";

				console.log(globalName);
			};

			myNewFunction2();

			// console.log(nameInside); ---cannot invoke bec it's outside {}


		// Using alert ()
			// alert() allow us to show a small window at the top of our browser page to show information to our users. As opposed to console.log
			// hold process or stop until you dismiss it

			alert("Hello World");

			
			// other sample with function, lalabas sya after you clock alert
			function showSampleAlert() {
				alert("Hello, User");
			};

			showSampleAlert();

			console.log("I will only log in the console when the alert is dismissed");


		// Using prompt
			// prompt() allow us to show a samll window at the browser to gether user input. It, much like alert will have the page wait until it is dismissed

			let samplePrompt = prompt("Enter your name: ");

			console.log("Hello, " + samplePrompt);

			let sampleNullPrompt = prompt("Don't enter anything.")

			console.log(sampleNullPrompt);
			// return an empty string when there is no input. or null if the user cancels the prompt().

			function printWelcomeMessage() {
				let firstName = prompt("Enter your first name: ");
				let lastName = prompt("Enter your last name: ");

				console.log("Hello, " + firstName + " " + lastName + "!" );
				console.log("Welcome to my page!");
			};

			printWelcomeMessage();

			// Function Naming Convention

			// Function Names shuld be definitive of task it will perform. It usually contains a verb.

			function getCourses() {
				let courses = ["Sciene 101", "math 101", "Englich 101"]
				console.log(courses);

			};

			getCourses();

			// Avoid generic names to avoid confusion

			function get() {
				let name = "Jamie";
				console.log(name);
			};

			get();

			// Avoid pointles and inappropriate functiona and names

			function foo() {
				console.log(25%5);
			};

			foo();

			// Name yoor functions in small caps. follow camelCase when naming variables and functions

			function displayCarInfo() {
				console.log("Brand: Toyota");
				console.log("Type: Sedan");
				console.log("Price: 1,500,000");
			};