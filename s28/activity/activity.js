// Insert One Document
			db.rooms.insertOne({
				"name": "single",
				"accomodates": 2,
				"price": 1000,
				"description": "A simple room with all the basic necessities",
				"room_available": 10,
				"isAvailable": "false"
			});

// Insert multiple room 
			db.rooms.insertMany([
				{
				"name": "double",
				"accomodates": 3,
				"price": 2000,
				"description": "A room fit for a small family going on a vacation",
				"room_available": 5,
				"isAvailable": "false"

				},
				{
				"name": "queen",
				"accomodates": 4,
				"price": 4000,
				"description": "A room with a queen sized bed perfect for a simple getaway",
				"room_available": 15,
				"isAvailable": "false"
				}
			]);


// find method
				
				db.rooms.find({name: "queen"})

// updateOne method

				db.rooms.updateOne({room_available: 15}, 
					{$set: {room_available: 0}})

// deleteMany method

				db.rooms.deleteMany({rooms_available:0})


