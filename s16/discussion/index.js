console.log("Hello World!");

// Arithmetic Operator

	let x =1397;
	let y = 7831;

	let sum = x+y;
	console.log("Result of addition operator: " +sum);


	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x* y;
	console.log("Result of product operator: " + product);

	let qoutient = x / y;
	console.log("Result of qoutient operator: " + qoutient);


	let remainder = y % x;
	console.log("Result of remainder operator: " + remainder);

	// Assignment Operators

		// Basic assignments operator (+)
		let assignmentNumber = 8;

		// Addition asignment operator (+=)
		// The addition assignment operator the value of the right operand to avaraible and assigns the result to the variable



		assignmentNumber = assignmentNumber + 2;
		console.log("Result of addition assignment operator: " + assignmentNumber);

		// Shorthand 
		assignmentNumber += 2;
		console.log("Result of addition assignment operator: " + assignmentNumber);


		assignmentNumber -= 2;
		console.log("Result of subtraction assignment operator: " + assignmentNumber);

		assignmentNumber *= 2;
		console.log("Result of operator assignment operator: " + assignmentNumber);

		assignmentNumber /= 2;
		console.log("Result of division assignment operator: " + assignmentNumber);


// Multiple Operators and parentheses

		/*
			-When multiple operators are applied in single statement, it follows the PEMDAS (Parehtheses, Exponent, Multiplication, Division, Addition, Subtraction)


		*/

		let mdas = 1 + 3 -3 * 4 / 5
		console.log("Result of mdas operations: " +mdas);


		let pemdas = 1 + (2 - 3) * (4 / 5);
		console.log("Result of pemdas operations: " +pemdas);


// Increment and Decrement

		let z = 1;

		let increment = ++z;

		console.log("Result of increment: " +increment);


		let decrement = --z;
		console.log("Result of decrement: " +decrement);


// Type Coercion
		/*
			-Type Coercion is the automtic or implicit conversion of values from one dta type to another
			-This happens when data types that would normally not be possible and yield irregular results.
			-Values are automatically converted from one data type to anothere in order to resolve operations
		*/


		let numA = '10';
		let numB = 12;


		// Adding/concatenating a string and a number will result in a string

		let coercion = numA + numB;
		console.log(coercion);
		console.log(typeof coercion)

		let numE = true +1;
		console.log(numE);

	// true = 1, false = 0

		let numF = false +1;
		console.log(numF);


// COmparison Operator
	
	let juan = "juan";

	// Equality Operator (==)
	/*
		-checks wether the oprands are equal/have the ame content
		-returns a boolean value
	*/
	console.log( 1 == 1 );
	console.log( 1 == 2);
	console.log( 1 == '1');  
		// not strict in terms of data 
	console.log(0 == false);
	// compares two string that are the same\
	console.log('juan' == 'juan');
	// compares the string with the variable "juan" declared above
	console.log('juan' == juan);


	// Inequality Operator
	/*
 		-checks wether the operands are not equal/have different content
	*/

	console.log("start of inequality operators: ")
	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);


	// Strict Equlity Operator
	/*
		-Checks wether the oprand are equal/have the same content
		-aLSO compares THE DATa TYPES OF 2 TYPES VALUES
		-JavaScript is a loosy typred language meaning that values of different data ttypes can be stored in variables
		-This sometimes cn cause problems within our codes
	*/

	console.log("START WITH STRICT EQUALITY OPERTOR: ")
	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);

	// STRICT INEQUALITY OPERTOR

	console.log("START WITH STRICT INEQUALITY OPERTOR: ")
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

// Relational Operator
	// Some comparison operators check wether one values is greater or less than to the other value

	let a = 50;
	let b = 65;

	// GT or Greater Than operator (>)
	let isGreaterThan = a > b;
	// LT or Less Than operator (<)
	let isLessThan = a < b;
	// GT or Greater Than or Equal to (>=)
	let isGTorEqual = a >= b;
	// LT or Less Than or Equal to (<=)
	let isLTorEqual = a <= b;

	console.log("START OF RELATIONAL OPERATORS")
	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);


// logical Operators
	
	let isLegalAge = true;
	let isRegistered = false;

	// logical and Operator (&& - Dounle Amersand)
	// Returns true if all operands are true

	let allRequirementsMe = isLegalAge && isRegistered;
	console.log("Result of Logical AND operator: " + allRequirementsMe);


	// Logical OR operator (|| - double pipe)
	// Returns true if one of the operands are true

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of Logical OR OPERTOR: " + someRequirementsMet);

	// Logical NOT operator (! -Exclamation Point)
	// Returns the oppsite value

	let someRequirementsNotMet = !isRegistered;
	console.log("Result of Logical NOT operator: " +someRequirementsNotMet);
