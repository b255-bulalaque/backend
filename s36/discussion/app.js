// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows ud to use all of the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database
// Connecting MongoDB atlas

mongoose.connect("mongodb+srv://shiela-255:admin123@zuitt-bootcamp.ulji5nu.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);


// add the task route
// allows all the task route created in the "taskRoute.js" file to use the"/task" route
app.use("/tasks", taskRoute);






// Server Listening 

if(require.main === module){
	app.listen(port, () => console.log(`Server running at ${port}`))
}

module.exports = app;
