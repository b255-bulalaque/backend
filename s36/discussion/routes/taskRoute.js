// Contains all the end points of our application
// We seperate the route such that "app.js" only contains information on the server
// We need to use the expresss Router() function to achieve this

const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows  access to HTTP method middlewares that makes it easier to crete routes for our application
const router = express.Router();


// the "taslController" allows us to use the functions defined in the "taskCOntroller.js file"
const taskController = require("../controllers/taskController");

/*
	-[SECTION] ROUTES
	-The Routes are responsible for defining the URIs that our client access and the corresponding controller tht will be used when the route is accessed
	-Tehy invoke the controller functions from the controller files
*/

// Route to get all the tasks
// This route expects to receive a get request at the URL "/"
router.get("/", (req,res) =>{

	// Invoke the "getAllTasks" function from the "taskController.js" file and sends the result to postman
	// 'ResultFromController' is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name fo a result using the parameter name "result/res"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a task
router.post("/", (req,res) =>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// Route to delete a task
// The task id is obtain from the URL is donated by the ":id" identifier in the route
// The colon (:) is an identifier that helps create a dynamiv route which allows us to supply informartion in the URL
router.delete("/:id", (req,res) =>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


router.put("/:id", (req,res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// START OF ACTIVITY

router.get("/:id", (req,res) =>{

	taskController.getTasks(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req,res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// USe module.exports to exports the router object to use in the "app.js"
module.exports = router;