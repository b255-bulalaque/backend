// Controller contain the function and busines logic of our express.js application
// Meaning all  the operations it can do will be placed in this file


// USes the "require" directive to allow sus to the Task Model which allows to access mongoose methods to perform CRUD operations
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task") 



// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" and export this functions
module.exports.getAllTasks = () => {
	// The return statements returns the result of the mongoose method "find" back to the "taskRoute.js" file which incokes this function when the "/" route is accessed
	return Task.find ({}) .then (result => {
		return result;
	})
}


// Controller option for creating a task 
// The request body coming from the client was passes from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a"requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {
	
	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task ({
		// Set the "name" property with the value received from the client/POstman
		name: requestBody.name
	})


	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method awaits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/postman
	return newTask.save().then((task,error) => {
		if (error){
			console.log(error);
			
			// If an error is encountered the "return" statement wil prevent any oyher line or code below it and within the same code block from executing
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	// The "findByIdAndREmove" Mongoose methos will look for the task with the same ID provided from the URL and remove/delete it
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=> {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}


module.exports.updateTask = (taskId,newContent) => {
	return Task.findById(taskId).then((result,error) =>{
		if (error){
			console.log(error);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
			return false;
			} else {
				return updatedTask;
			}
			

		})
	})
}

// START OF ACTIVITY
module.exports.getTasks = (taskId) => {
	// The return statements returns the result of the mongoose method "find" back to the "taskRoute.js" file which incokes this function when the "/" route is accessed
	return Task.findById(taskId) .then (result => {
		return result;
	})
}


// put method automatically change the status of Id
module.exports.updateTask = (taskId,complete) => {
	return Task.findById(taskId).then((result,error) =>{
		if (error){
			console.log(error);
			return false;
		}
		result.status = "complete";
	
		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
			return false;
			} else {
				return updatedTask;
			}
			

		})
	})
}

