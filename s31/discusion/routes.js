const http = require('http');

// Creates a variable port store the port number
const port = 4000;

// Creates a vriable "server" that store the output of the "createServer" methos
const server = http.createServer((request,response) => {
	if (request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Hello Again')
	} else if (request.url == '/homepage'){
			response.end('This is the homepage')
	} else	{
			response.writeHead(404, {'Content-Type': 'text/plain'})
			response.end('404 - Page not available')
	}
});

// Uses the "server" and "port" variables created above
server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);

// nodemon routes.js

// if gusto nating walang url - we only put /
/*if (request.url == '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Hello Again')*/