// "require" directive - used to load node.js modules
// "http module" - lets the node.js transfer data using the HTTP
// "HTTP" - protocol that allows the fetching of resources
/*
	we ae now able to run simple node.js server.Wherein when we added ou URL in the browser, a client actually requested to our server and our erver was able to respond with a text

	-we used require method to load node,js modules
	-a module isa software component or part of a program which contains one ormore routines 

	the http module is a default model from node.js
	the http module let node.js transfer data or let our client and server exhange data via hypertext transfer protocol
*/

let http = require("http");

/*
http.crreateServer() method allows us to create a srver anf handl the request of a client

request - messages sent by the client via a web browser

response - messages sent by the server as an answer

*/

http.createServer(function(request,response){

		// res.writeHead is a method of a response object this will allow us to add header, which are additional information about our server's response. 'Content-Type' is one of the more recognizable headers, it is pertaining to the data of the content we are responding with. The first arguments in ritehead is an HTTP which is used to tell the client about the status of their request. 
	// 200 meaning OK.
	// HTTP 404 means the resource you;re trying to access cannot be found
	// 403 means the resouce the resource you're trying to access is forbidden or requiring authentication
	
	response.writeHead(200, {"Content-Type": "text/plain"});

	// res.end() is a method of the respond object which ends the server response and send the message/data as a string


	response.end("hello world");


// /listen() allows us to assign a port to a server.
	// port is a virtual point where connections start and end
	// http://localhost:4000 -localhost is your current and 4000 is the port number assigned to where the process
	// server is listening or running from. port 4000- popularly used for backend applications 
}).listen(4000)

// When server is running console will printout the message
console.log('Server is running at localhost:4000')


// REMINDER: DO NOT CLOSE THE GITBASH/TERMINAL
// node index.js
