/*The questions are as follows:

a. What directive is used by Node.js in loading the modules it needs? 
Answer: Require directive

b.What Node.js module contains a method for server creation?
Answer: http method

c.What is the method of the http object responsible for creating a server using Node.js?
Answer: http. createServer() 

d.What method of the response object allows us to set status codes and content types?
Answer: response.writeHead()
e. Where will console.log() output its contents when run in Node.js?
Answer: When server is running console will printout the message

f.What property of the request object contains the address' endpoint?
Answer: response.end()
*/
