let http = require("http");

// mock database

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
	"name": "Jobert",
	"email": "jobert@mail.com"	
	}
]

http.createServer(function(request,response){

	// route for returning all items upon receiving a GET request
	if (request.url == "/users" && request.method == "GET"){
		response.writeHead(200,{'Content-Type': 'application/json'});

		// Inpur HAS to be dta type STRING hence the JSON.stringify() method
		// This tring input will be converted to desired output data type which has been sent JSOn
		// This is done because request and responses sent between clint and Node.js server requires the informtion sed and received a stringified JSON
		response.write(JSON.stringify(directory));
		response.end();
	}

	// A request objct contains several parts
		/*
			-Headers contains information about the request context/content like that what is data type
			-Body conains the actual information being sent with the request

		*/
	if(request.url == "/users" && request.method == "POST"){


		let requestBody = '';

		// data id received from htclinet and it is processed in the data stream
		// The information provided from the request object enters a sequence called "data" the code below will be triggered 
		request.on('data', function(data){

			// Assigns the data retrieved from the data stream to the requestBody
			requestBody += data;
		});

		request.on('end', function(){
			// We need this to be of data type JSON to access its properties
			console.log(typeof requestBody);

			// COnvert the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock data base record

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})

	}
}).listen(4000);

console.log('Server running at localhost:4000');

