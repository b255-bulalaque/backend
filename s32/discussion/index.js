// Use the require directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// the "HTTP module" lets Node.js transfer data using hyper text transfer protocol
// HTTP is a protocol tht allows the fetching the resources such as HTML documnets
// Clients (browser) and servers (node.js/express js application ) communicate by exchanging individual messages
// The messages sent by the client, usually a web browser, are called requests
// the messages sent by the serve as an answer are called responses

let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses
// http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are requests and response objects(data type) that contains method taht allows us to receive requests from the client and send responses back
http.createServer(function(request,response){

	// The http method of the incoming request can be accessed via the "method" property of the "request" parameter
	// The metod "GET" means that we will retrieving or reading information 
	if (request.url == "/items" && request.method == "GET"){

		// Request the "/items" path and "GETS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// Ends the response process
		response.end('Data retrieved from the database');
	}


	// The method "POST" means that we will be adding or creating information
	// In this example, we will just be sending a text response for now
	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database');
	}
}).listen(4000);

console.log('Server is running at localhost:4000')