console.log("Hello World");

// [SECTION] JSON Objects
/*
	-JSON stands for JavaScript Object Notation
	-JSON is also used in other programming languages hence he name JavaScript Object notation
	-Core JavaScript ha sbuilt in JSON object that contains methods for parsing JSON objects and coverting string into JavaScript objects
	-JavaScript objects are not to be confused with Jayson
	-Serialization is the process of converting data into a series pf bytes fpr easier transmission/transfer of information
	-A bytes is a unit of data that is eight binary digits(1 and 0) that is used to represent a character (letters, numbers, or typographics symbols
	-Bytes are information that a computer processes to perform differrent tasks)
*/

// JSON Objects
/*
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}
*/

// JSON Arrays

/*
"cities": [
{"city": "Quezon City", "province": "Metro Manila","country": "Philippines"},
{"city": "Manila City", "province": "Metro Manila","country": "Philippines"},
{"city": "Makati City", "province": "Metro Manila","country": "Philippines"}
	]
*/


// [SECTION] JSON methods
// - The JSON object contains methods for parsing and converting data into stringified JSON

// [SECTION] Converting Data into stringified JSOn
/*
	-Stringified JSON is a JAvaSript object converted into a string to be used in other functions of a JavaScript application
	-They are commonly used in HTTP request where information is required to be sent and received in a stringified JSON format
	-Requests are important part of programming where an application communicates with a backend application to perform different task such as getting/creating data in a database
	-A frontend application is an application that is used to interat with user to perform different visual task and display information while backend applications are commonly used for all the business logic and data processing
	-A database normally stores information/data that can be used in most applications
	-Commonly stored data in databases are user information, transaction records and product information
	-Node/Express JS are two types of trchnologies that are used for creating backend application which processed requests from frontend applications
*/



let bactchesArr = [{batchName: 'Batch X'},{batchName: 'Batch Y'}]
console.log('Result from stringify method: ')
console.log(JSON.stringify(bactchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
	city:'Manila',
	country: 'Philippines'
	}
});
console.log(data);


// [SECTION] Using stringify method using variables 
/*
	-When inofrmation is stored in variables and is not hard coded into an object that is being stringified, we can supply the value with a variables
	-The property and a "value" would have the same name which can be confuding for beginners
	-This is done on purpose for a code readability meaning when and information is stored in a variable anf whrn thr objects created to be stringified is created, we supply the variable name instead of a hard coded value
	-This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
*/

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your country belong to??')

};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);



// [SECTION] Coverting stringified JSON into a Javascript object
/*
	-Objects are common data types used in application beacuse of the compex data structure that can be created out of them
	-Information is commonly sent applications in stringified JSON and then converted back into objects
	-This happens when both for sending information to a backend application
*/

let batchesJSON = '[{"batchName": "Batch X"},{"batchName": "Batch Y"}]';
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));