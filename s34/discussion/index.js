// npm init
// npm install express
// add file .gitignore
/*
	-Use the 'require' directive to load the expres module/package
	- A "module" is a software component or a part of  program that contains one or mor eroutines
	-This is used to get the contents of the express package to be used by our application 
	-Its also allows us access to methos and functions that will allow us to craete a server
*/
const express = require("express")


/*
	-Create an application using express
	-This creates an express application ans stores this is an constant called aoo
	-In layman's term app is oour server
*/
const app = express ()


/*
	-For our application server to run, we need a port to listen
*/
const port = 4000;


/*
	-Setup for allowing the server to handle data from requests 
	-Allows your apo tp read json data
	-Methods used from express JS are middlewares
	-Middleware is a software that provides common servies and capabilities to applications outside of what's offered by operating system
*/
app.use(express.json());



/*
	-Allows your pp to read data from forms
	-By default, information received from the URL can only be reveived as a string or an array
	-By applying the option of "extended:true" this allows us to receive information in other data types such as an object which will use throughout our application
*/
app.use(express.urlencoded({extended:true}));

/*
	[SECTION] ROUTES
	-Express has a methods corresponding to each HTTP method
*/

app.get("/", (req, res) => {
	res.send ("hello world")
});

// [MINI ACTIVITY] Create anothe GET route
// Our route must be named "hello"
// it must output this message "Hello from the /hello endpoint"

app.get("/hello", (req, res) => {
	res.send ("Hello from the /hello endpoint")
});



// Create a POST ROUTE

app.post("/hello", (req,res) => {
	// req.body contains the contents/data of the request body
	// All the properties defined in our Postman request will be accesible here as properties with the same names
	res.send(`Hello There ${req.body.firstName} ${req.body.lastName}`

	)});

// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database

let users = [];
app.post("/signup", (req,res) => {
	console.log(req.body);

	// if contents of the "request.body" with property "userName" and "password"is not empty
	if (req.body.userName !== '' && req.body.password !== ''){
		users.push (req.body);
		res.send(`User ${req.body.userName} succesfully registered!`);
	} else {
		res.send ("Please input BOTH username and password")
	}
});

// Create PUT route to change password of a specific user

app.put("/change-password", (req,res) => {
	let message;


	// Creates forloop that will looop through each element of the array
	for (let i=0; i < users.length; i++){

		// If the username provided in the postman client and the username of the current object in the loop is the same
		if (req.body.userName == users[i].userName){

			// Changes password of the user found by the loop
			users[i].password = req.body.password;
			message = `User ${req.body.userName}'s password has been updated`;
			users.splice(1);
			message = `User ${req.body.username} has been deleted`

			// Breaks out of the loop once a users that matches the username provided is found
			break;

		} 
	}
	res.send(message);
});

/*
	-Tells our server to listen to the port
	-If the port is accessed, we can run the server
	-Returns a message to confirm that the server is running in the terminal

	- if(require.main) would allow us to listen to the app directly if it is not imported to another modules, it will run the app directly
	-else, if it is needed to be imported, it will run the app and instead export it to be used another file
*/

if (require.main === module){
	app.listen(port, ()=> console.log(`Server running at port ${port}`))
}

module.exports = app;