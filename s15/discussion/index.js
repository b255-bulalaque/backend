// [SECTION] Syntax, Statements and Comments
// Statements in programming are instruction that we tell computer to perform
// JS statement usually end with the semicolon (;) 


console.log("Hello World!");

// comments are prts of the code that gts ognored by the language
// commnets are meant to described the writen code

/*
There are two types of comments:
	1. The single-line comment denoted by two slashes
	2. the multi-line denoted by a lash and an sterisk
*/

// [SECTION] Variables
// Any information that is used by an application is stored in what we call a "memory"
// When we create a variables, certain portions of adevices memory is givem "namr that we call "variables"

// Decalring Variables
// Declaring Variables-tell us our device that an variable names is created and ready to store data
// Declaring a variable without assigning a value will automatically give it the value of "undefined"

let myVariable;
console.log(myVariable);
// console.log () is useful for printing ou values of variables or certain result of code into the consoel.

let hello;
console.log(hello);
// Variable must be declared first before they are used
// Using variables before they're declared will return an error

/*
	Guide in writing varibles:
	1. Use the 'let' keyword followed by the variable name of your choosing nad use the assignment opertor (=) to assign a value
	2. Variable means should start with a lowercase character, use camelCase for multiple words
	3.For constant variables, use 'conts' keyword'
	4. Varaiables names should be indicative (or descriptive) of the value stored to avoid confusion
*/

// Declaring and initializing Variables
// Initializing variables- the instance when variable is given it's initial/starting value

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice)

// in the context of certain application some variable information are constant that should not be change
// in this example, the interest rate for a loan savings, account or a mortage must not be changed due to real world concerns.


const interest = 3.539;

// Reassigning variables values
// Reassigning a variables means changing it's initial or previous values into another value

productName = 'Laptop';
console.log(productName);

// Reassigning variables vs Initializing variables
// Decalred a variable first
let supplier;
// initialization is done after the variable has been declared
// this is considered as initialization because it is the first time the value has been assigned to a variable
supplier = "John Smith Tradings";
console.log(supplier);

// This is considered a reassignment because it's initial value was already declared
supplier = "Zuitt Store";
console.log(supplier);

/* const pi;
pi = 3.14;
console.log(pi);*/

// var vs. lest/const
	
		// var- is also used in declaring variables but var is an ECMAScript (ES1) feature (JavaScript 1997)
		// let/const was introduced as a new feature in ES6 (2015)

		// there are issued associated with variables declared with var regarding hoisting
		// Hoisting JavaScript default behavior of moving declaration to the top
		// In terms of variable and constants, keyword var is hoisted and let and const does not llow hoisting

		// for example
		a = 5;
		console.log(a);
		var a;

		// in the above example, variable a is used before declaring it. and the program works and display the output of 5

		// let/const local/global scope
		// Scope essentially means where these variables are available for use
		// let and const are block scoped
		// A block is a chunk of code bounded by {} A block live in curly braces. Anything within curly braces is a block.

// Multiple variable declaration
// Multiple varaibles maybe declared in one line
		// though it is quicker to do without having retype th "let" keyword, it is still the best practice to use multiple "let/const" keywords

let productCode = 'DC017', productBrand = "Del";
console.log(productCode, productBrand);

// [SECTION] Data Types

// Stings are a series of chracter that creates a word, a phrase, a sentence, or anything related to creating text
// String in JavaScript can written using either sing ('') or dounle ("") qoute

let country = 'Philippines';
let province = "Metro Manila";


// Concatenating STrings
// Multiple string values can be combine to create a single string using "+" symbol
let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = 'I live in the '+country;
console.log(greeting);

// the escape character (\) in string in combination with ither character can produce different effects
// the "/n" refers to creating a new line in between the text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// numbers
// intergers/whole numbers

let headCount = 26;
console.log(headCount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);


// boolean
// Boolean values are normally used to store values relating to the stte of  certain things
// this will be useful in further discussions about creating logic to make our applications respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are spoecial kind of data type that's used to store multiple values
// arrays can store different data type but is normally used to store similar data type

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
// storing different data tyes inside array is not recommneded because it will not make sense in the context of programming
let details = ["John", "Smith", 32, true];
console.log(details);

// objects
// objects are another special kind of data type tht used to mimic real world onject/ items they are used to vreate complex dta that contains pieces of information that relevant to each other
// every individual piece of inofrmation is call a property of the object

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

// theyre laso useful for creating bstract objects

let myGrades = {
	frirstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}

console.log(myGrades);


// typeof operator is ude to determine the type of ata or the value of a variable

console.log(typeof myGrades);

const anime	= ['one piece', 'one punch man', 'attack on titan'];
console.log(anime[0]);

anime[0] = 'kimetsu no yaiba';
console.log(anime);

// Null
// it is used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;
// using null compared to a 0 value is much better for readability purposes
// null is also considered to be a data typ[e of its own compared to 0 which is data type of number

// Undefined
// Represents the states o a variable that has been declared but without as assigned value

let fullName;
console.log(fullName);


// undefined vs null
// one clear difference between ubdefined and null is that for undifine, a varaible was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any amount
