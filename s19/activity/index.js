console.log('Hello World');

let username;
let password;
let role;



function login() {
	username = prompt('Enter your username: ').toLowerCase();
	password = prompt('Enter you password: ').toLowerCase();
	role = prompt('Enter your role: ').toLowerCase();
	
	if (username === '' || password === '' || role === '') {
		 console.log('inputs must not be empty');
		// alert('inputs must not be empty');
		 // return "inputs must not be empty";
	}else {
		switch (role){
			case 'admin': 
				console.log('Welcome back to the class portal, admin!');
				break;
			case 'teacher': 
				console.log('Thank you for logging in, teacher!');
				break;
			case 'student': 
				console.log('Welcome to the class portal, student!');
				break;
			default:
				console.log('Role out of range');
				break;
		}
	}
};
login();


let num1;
let num2;
let num3;
let num4;

function checkAverage() {
	let num1 = parseInt(prompt("Enter your first grade: "));
	let num2 = parseInt(prompt("Enter your second grade: "));
	let num3 = parseInt(prompt("Enter your thirs grade: "));
	let num4 = parseInt(prompt("Enter your fourth grade: "));
	
	return Math.round(num1+num2+num3+num4)/4;
}

let average = checkAverage();
if (average <= 75) {
	console.log('Hello, student, your average is '+average + ' The letter equivalent is F');

} else if (average >= 75 && average <= 79){
		console.log('Hello, student, your average is '+average + ' The letter equivalent is D');
	} else if (average >= 80 && average <= 84)
	{
		console.log('Hello, student, your average is '+average + ' The letter equivalent is C');
	} else if (average >= 85 && average <= 89)
	{
		console.log('Hello, student, your average is '+average + ' The letter equivalent is B');
	} else if (average >= 90 && average <= 95){
		console.log('Hello, student, your average is '+average + ' The letter equivalent is A');
	};


 

//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}