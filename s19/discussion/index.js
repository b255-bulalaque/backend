console.log('Hello World');

// Conditional Statemnt allows us to control the flow of our program. t aloows us to run a statemnt or instruction if a condition is met another seperate instruction if otherwise

// [SECTION] if, else if, statemnt
let numA = -1;

if (numA < 0){
	console.log('Hello');
};

// The result of the expression added in the condition in theif's condition must result to the true, else, the statemnt insdie if () will not run

// You can also check the condition. The expression results to a boolean true because of the use of the less than operator.
 	console.log(numA<0);
 	 // results to the true and som the if statement was run.


numA = 0;

if (numA < 0){
	console.log("Hello again if a numA is 0!");
};

// It will not run because the expression now result to false.
console.log(numA < 0);

let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York city!");
};

// Else it clasue

/*
	-Execute a statement if previous condition are false and ig the specified condition is true 
	-The "else if" clasue is optional and can be added tocapture additionAL conditions to change the flow of a program

*/

let numH = 1;

if(numA < 0) {
	console.log('Hello');
} else if (numH > 0){
	console.log ("World");
}

// We were able to run the else if() statemnt afte rwe evaluated that the if consdition was failed

// If the if() condition was passed and run, we will no longer ebaluate the else if() and end the process there.

numA = 1;
if (numA >0){
	console.log('Hello');
} else if (numH > 0){
	console.log('World');
}

// else if() statement no longer ran because the if statement 

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York city");
} else if(city === "Tokyo") {
	console.log("Welcome to Tokyo,Japan!")
}

// Since we failed the condition for the first if(), we went to the else if() and cheked and passed that condition

// Else statement 

/*
	-Executes a statement if all other conditions are false
	-The "else" statement is optional and can be added to capture any other result to change the flow of the program
*/

if (numA < 0){
	console.log('Hello');
} else if (numH === 0) {
	console.log('World');
} else	{
	console.log('Error! numA is not less than 0');
}

// Else statemnts should only be added if there is a preceeding if condition. Else statement by itself will not work, however, if statement will work even if there is no else statement

// if, else if and else statements with functions 
/*
	-Most of the time we would like to use if, else if and else statemnt with functions to control the flow of our application
	-By including them inside functions, we can decide when certain condition will be checked instead of executing statements when the JavaScript loads
	- The return statement can be utilized with conditional statements in combination with function to change values to be used for other feautures
*/

let message = 'No Message';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30){
		return 'Not a typhoon yet';
	}
	else if (windSpeed <=61){
		return 'Tropical depression detected';
	}
	else if (windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropica storm detected'
	}
	else if (windSpeed >= 89 || windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else {
		return 'Typhoone detected'
	}

}

message = determineTyphoonIntensity(50);
console.log(message)


// MINI ACTIVITY
// Create a function with an if else statement inside
// The function should test for if a given number is an even or an odd number
// Print  "Number is even//divisible by 2
// Print  "Number is odd// if the number has a remainder when divided by 2

function givenNumber(number) {
	if (number % 2 === 0){
		console.log("Number is even");
	} else {
		console.log("Number is odd");
	}
}

givenNumber(10);


//Truthy Examples
/*
	-If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statemnts are executed
	-Expression are any unit of a code that can be evaluated to a value
*/ 

if (true) {
	console.log('Truthy');
}
if (1) {
	console.log('Truthy');
}

if ([]) {
	console.log('Truthy');
}

// Falsy examples
if (false) {
	console.log('Falsy');
}
if (0) {
	console.log('Falsy');
}

if (undefined) {
	console.log('Falsy');
}

// [SECTION] COnditional (ternary) Operator
/*
	-The conditional (Ternary) Operaror takes in three operands
	1. condition
	2. expressiion to execute if the condition is truthy
	3. expression to execute if the condition is falsy
	-can be used as an alternative to an "if else" statement
	-commoni=ly used for single statement execution where the result consists of inly one line of code

	Syntax
	(Expresssion) ? ifTrue : ifFalse;
*/

// Single statement execution
let ternaryResult =(1<18) ? true : false;
console.log("Result of Ternary operator: " + ternaryResult);

// Multi Statement execution

let name;

function isOfLegaLAge() {
	name = 'John'
	return 'You are under the age limit'
}

function isUnderAge() {
	name = 'Jane'
	return 'You are under the age limit'
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age > 18) ? isOfLegaLAge() : isUnderAge();
console.log("Result of Ternary operator in functions: " +legalAge+ ", " +name)


// [SECTION] Switch Statements

/*
	-The switch statemnt evaluate an expression and matched the expressions value to a case clause. The switch will then execute the statement associated with that case, as well as statements in cases that fllow the match
	-switch cases are considered as "loops"
	meaning it will compare the expression with each of the case values until a match is found 
	- the "break" statements is used to terminate the current loop once a match has been found
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is red");
		break; 
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input valid day");
		break;
}

// [SECTION] Try-catch-finally statements
/*
	-"try catch" statements are coomonly used for error handling
	-There are instances when the application returns an error/warning that is not necessarily an error in the conntext or our code
	-These errors are a result of an attempt of the programming language to help developers in creating efficient code
	-They are used to specify a response whenever an exception/error is received
*/

function showIntensityAlert(windSpeed) {
	try{
		alerat(determineTyphoonIntensity(windSpeed));

	// error/err are commonly used varibale names used by developers for storing errors
	} catch (error) {
		// The type of operator is  used to check the data type of a value/expression and returns a string value of what the data type is
		console.log(typeof error);
		// catch errors within the try statemnet
		// In this case the error is an unknown function 'alerat' which does not exist in JavaScropt
		// "error.message" is used to access the information relating to an error object
		console.warn(error.message);
	} finally {
		// Continue execution of code regardless of succss and failure of code execution in the 'try' block to hande/resolve errors
		alert('Intensity updates will show new alert');
	}
}

showIntensityAlert(56);