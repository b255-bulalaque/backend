console.log("Hello World");

// Exponent Operator
let getCube = 2**3
console.log(`The cube of 2 is ${getCube}`);

// Template Literals

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];



console.log(`I live at ${address[0]} ${address[1]} ${address[2]} ${address[3]}`)
// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
console.log(`${animal.name} was a ${animal.species} crocodile. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => {
	console.log(`${numbers}`)
});

// Javascript Classes

class Dog {
	constructor(name,age,breed){
	this.namr = name;
	this.age = age;
	this.breed = breed;
	}
};
 const newDog = new Dog("Frankie",5,"Maniature Dachshud");
 console.log(newDog);



//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}