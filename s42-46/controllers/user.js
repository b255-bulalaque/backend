// the "User" variable is defined using a capitalized letter yo indicate that what we are using is the "USer" model code readability
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
//  npm install bcrypt- encrypting password 
const auth = require("../auth");


// Check if email already exist
/*
	Steps:
	1. USe mongoose "find" method to duplicate emails
	2. USe the "then" method to send a respond back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the frontend via the "then" method found in the route file 
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};

// User Registration
/*
	Steps:
	1/Create a new User object using the mongoose model and the information request body
	2. MAke sure that the password is encryptrd
	3.Sve the ne User to the databse
*/
module.exports.registerUser = (reqBody) => {
  // Check if user with the same email already exists
  return User.findOne({ email: reqBody.email }).then((existingUser) => {
    if (existingUser) {
      // User with the same email already exists
      return { error: "Email already registered" };
    } else {
      // Create a new user object and save it to the database
      let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
      });

      return newUser.save().then((user) => {
        // User registration succeeded
        return  user;
      }).catch((error) => {
        // User registration failed
        return { error: "Registration failed" };
      });
    }
  }).catch((error) => {
    // Database error
    return { error: "Database error" };
  });
};



// User authentication

/*
	Steps:
	1.Check the database if the user email exist
	2, Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the same criteria
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of the comparing login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login from to the encrypted password retrieved from the database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled
				return {access : auth.createAccessToken(result)}
			// Passwords do not match
			} else {
				return ("Fail");
			}
		}
	})
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		if(result == null){
			return false
		} else {
			result.password = "";

			// Returns the user information with the password as an empty string
			return result;
		}

	});

};

// Set user as admin

module.exports.updateUser = (reqParams) => {

	let updateActiveField = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((user, error) => {

		// Product not archived
		if (error) {

			return ("ERROR!");

		// Product archived successfully
		} else {

			return ("Successful");

		}

	});
};
		


// Checkout

module.exports.userCheckout = async(data)=>{
    // Add the productID in the userOrders array of the user
    // Creates an "isUserUpdated" variable and returns upon successful update of otherwise false
    // Using the

    let isUserUpdated = await User.findById(data.userId).then(user=>{

        return user.save().then((user,error)=>{
            if (error){
                return false
            }else{
                return true
            }
        })
    })
     // Add the userID in the ordered product array of the product
     // Using the 'await' keyword will allow the checkout method to complete updating the product before returning a response back to the front end // 

     let isProductUpdated = await Product.findById(data.productId).then(product=>{
         // Adds the userId in the user's ordered array
         product.userOrders.push({userId: data.userId})
// Saves the updated product information in the database
         return product.save().then((product,error)=>{
             if(error){
                 return false
             }else{
                 return true
             }
         })
     })

     // Condition that willl check if the user and Course documents have been updated
     // User checkout successful

     if(isUserUpdated && isProductUpdated){
         return true

         // user checkout failure
     }else{
         return false
     }
};



// Retrieve all users
/*
	Steps:
	1. Retrieve all the usera from the database
*/
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}



// Set User as admin (Admin Onlu)
module.exports.userAdmin = (reqParams) => {

	let updateAdminField = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((user, error) => {

		// User set as admin unsuccessfully
		if (error) {

			return ("Fail");

		// User set as admin successfully
		} else {

			return ("User set as Admin");

		}

	});
};




 // Set Admin as user (Admin Onlu)
module.exports.user = (reqParams) => {

	let updateAdminField = {
		isAdmin : false
	};

	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((user, error) => {

		// User set as admin unsuccessfully
		if (error) {

			return ("Fail");

		// User set as admin successfully
		} else {

			return ("Admin set as User");

		}

	});
};






// Retrieve authenticated user's orders

module.exports.getOrders = (userId) => {
    return Product.find({userId: userId}).then(user => {
        if(user === null){
            return false;
        } else { 
            return user; 
        }
    })
}; 





// orders

/*module.exports.userOrder = (reqBody) => {
	// Create a variable "newOrder" and instantiates a new "Product" object using the mongoose model
	
		let newOrder = new Order({
			productId : reqBody.productId,
			productName : reqBody.productName,
			quantity : reqBody.quantity
				
	}) 
	
	
	

	// Saves the created object to our database
	return newOrder.save().then((order, error) => {
		//  failed
		if(error) {
			return false;

		//  successful
		} else {
			return true;
		}
	})
}			
*/

