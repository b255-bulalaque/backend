const Product = require("../models/Product");

// Create a new Product
/*
	Steps:
	1. Create a new Product object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/


// Controller for Creating Products (Admin Only)

module.exports.addProduct = (reqBody) => {
	// Create a variable "newProduct" and instantiates a new "Product" object using the mongoose model
	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	// Saves the created object to our database
	return newProduct.save().then((product, error) => {
		// creation failed
		if(error) {
			return ("Creation Failed");

		// creation successful
		} else {
			return ("Creation Successful")
		}
	})
}

// Retrieve all product
/*
	Steps:
	1. Retrieve all the product from the database
*/
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all ACTIVE products
/*
	Steps:
	1. Retrieve all the products from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
};

// Retrieving a specific product
/*
	Steps:
	1. Retrieve the product that matches the product ID provided from the URL
*/
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
}


// Update a product
/*
	Steps:
	1. Create a variable "updateProduct" which will contain the information retrieved from the reqeust body
	2. Find and udpate the product using the product ID retrieved from the params property and the variable "updateProduct" containing the information from the request body
*/

// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fiels/properties of the document to be updated
	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
		// Product is not updated
		if(error){
			return (" Product is not Updated");
		// Product updated successfully
		}else{
			return (" Product is Updated Successfully");
		}
	})
}

// Archive a product
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the product"isActive" status into "false" which will no longer be displayed in the frontend application whenever all active products are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		// Product not archived
		if (error) {

			return ("Failed");

		// Product archived successfully
		} else {

			return ("Product Archived successfully");

		}

	});
};


// Activate Product (Admin Only)
module.exports.activatePoduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		// Product not archived
		if (error) {

			return ("Failed");

		// Product activate successfully
		} else {

			return ("Product Activated Successfully");

		}

	});
};




// Retrieve Authenticated user's orders


