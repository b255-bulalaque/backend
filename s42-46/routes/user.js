const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

const auth = require ("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// ROute for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});


// Set user as admin route

router.put("/:userId/update", auth.verify, (req, res) => {
	userController.updateUser(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for checkout product
router.post("/checkout", (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id,
		productId: req.body.productId,
		
	}
	
	console.log(userData)
	if(userData.isAdmin == false){
		userController.userCheckout(data).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}


	
})


// Route for retrieving all the users
router.get("/allUser", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController))
});


// Route for Activate User as admin(admin Only)
// JWT Verification is needed for this route to ensure that a user is logged in before archiving the product
router.put("/:userId/admin", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	if(user.isAdmin == true){
		console.log(req.body)
	userController.userAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))

	} else{
		res.send(false);
	}
})

// Route for Activate User as admin(admin Only)
// JWT Verification is needed for this route to ensure that a user is logged in before archiving the product
router.put("/:userId/user", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	if(user.isAdmin == true){
		console.log(req.body)
	userController.user(req.params, req.body).then(resultFromController => res.send(resultFromController))

	} else{
		res.send(false);
	}
})




// Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.getOrders(userData.id).then(resultFromController => res.send(resultFromController));
});


// getallorders


/*
// Route for Creating Order 
router.post("/userOrder", (req, res) => {
		const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		userController.userOrder(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});

*/
// Allows us to export the "router" object that will be acessed in our "index.js" file


module.exports = router;
