const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


// Route for Creating Products (Admin Only)
router.post("/add", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	if(user.isAdmin == true){
		console.log(req.body)
		productController.addProduct(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});

// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all the ACTIVE products
// Middleware for verifying JWT is not required because users who are not logged in should also be able to view the products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving a specific product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and chages depending upon the information provided
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	// Since the product ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the product ID by accessing the request's "params" property which contains all parameters provided via the url
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})



// Route for updating a product information(admin Only)
// JWT Verification is needed for this route to ensure that a user is logged in before updating a product
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
})



// Route for Archive Poduct(admin Only)
// JWT Verification is needed for this route to ensure that a user is logged in before archiving the product
// Route for Creating Products (Admin Only)



router.put("/:productId/archive", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	if(user.isAdmin == true){
		console.log(req.body)
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else{
		res.send(false);
	}
})

// Route for Activate Poduct(admin Only)
// JWT Verification is needed for this route to ensure that a user is logged in before archiving the product
router.put("/:productId/activate", auth.verify, (req, res) => {
	
	const user = auth.decode(req.headers.authorization);
	if(user.isAdmin == true){
		console.log(req.body)
	productController.activatePoduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else{
		res.send(false);
	}
})








// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;