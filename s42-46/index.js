// In terminal instal npm -y && npm install express
// In terminal install npm install mongoose

const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
// Allows access to routes defines within our application
const userRoutes = require("./routes/user");
const productRoute = require("./routes/product");
const orderRoute = require("./routes/order");


// Creates an "app" varaible that stores the result of the "express" function that initializes our express application
const app = express();
app.use(cors());
// Connect to our MOngoDB App
// Login to MongoDB- click connect, then click connect application and copy connection string, dont forget to change your password

mongoose.connect("mongodb+srv://shiela-255:admin123@zuitt-bootcamp.ulji5nu.mongodb.net/?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Message to prompt if connected to MongoDB
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlast'));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included fro all user routes defined in the "user" route file
app.use("/users", userRoutes);
app.use("/products", productRoute);
app.use("/orders", orderRoute);





/*
	-If (require.main) would allow us to listen to the app directly if it is not imported to another, it will run the app directly
	-else, if it is needed to be imported, it will not run the app and instead export it to be used in another file
*/
if(require.main === module){
	
	/*-Will used the define port number for the application whenever an environment variable is available OR will use port 4000 is none is define
		- This syntax will allows flexibility when using the application locally or as a hosted application
	*/
	app.listen(process.env.PORT || 4000, () =>{
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	})
}




module.export = app;

// npm install cors
