// Everthing here consist of SCHEMA(blueprint of data)

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		// Requires the data for our field/properties to be included when crating a record
		// the "true" value defines if the field is required or not and the second element in the arrat is he message that will be printed out in our terminal when the data is not present
		required : [true, "Name is required"]
	},

	description : {
		type : String,
		required : [true, "Description is required"]
	},

	price : {
			type : Number,
			required : [true, "Price is required"]
			},
	// default means it will  be automatically true
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		// The "new Date()" expression isntantiates a new "date" that stores the current date and time whenever a product is created in our database 
		default: new Date()
	},

	userOrders : [
	{
		userId : {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'userId',
			required:true
		},
		orderId : {
			type: String,
		}
	}]
})


module.exports = mongoose.model("Product", productSchema);