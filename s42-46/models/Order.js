const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({

    userId : {
        type : String,
        required : [true, "UserId of order owner is required"]
    },

    productId : {
        type : String,
        required : [true, "ProductId belonging to order is required"]
    },

    quantity : {
        type : Number,
        default : 0
    },

    amount : {
        type: Number,
        default: 0,
        required: [true, "amount is required"]

    },

    purchasedOn : {
        type : Date,
        default : new Date()
    }
}, {timestamps : true});


module.exports = mongoose.model('Order', orderSchema);
