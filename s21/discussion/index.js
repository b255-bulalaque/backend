console.log("Hello World!");

// An array in programmming is imply a list of data

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926',];

// [SECTION] Arrays

/*
	-Arrays are used to store multiple related values in a single variable
	-They are declared using square bracket [] also known as "array literals"
	-Arrays are commonly used to store numerous data to manipulate in order to perform a number of task
*/

// Common examples for Array

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands =['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu' ];

// Possible use of an array is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write an array
let myTasks = [
		'drink html', 
		' eat javascript',
		'inhale css',
		'bake sass'
	];

// Creating an array with values from variables
let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];
console.log(myTasks);
console.log(cities);


// [SECTION] length property
// the .length property allow us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blackArr = [];
console.log(blackArr.length);

// length property can also be used with string, some array methods and properties can also be used with strings.

let fullName = 'Jamie Noble';
console.log(fullName.length);


// length properties can laso set the total numbers of items in an array, we can actually delete the last item in the array

// original variable = original variable -1 (Remove array)
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// To delete a specific item in an array we can employ array methods (which will be shown in the next session)

// Another example of using decrement

cities.length--;
console.log(cities);

fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// If you can shorten the array by setting the length property you can laso lengthen it by adding a number by adding a  number into the lenth property

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++
console.log(theBeatles);

// [SECTION] Reading from arrays

/*
	-Accessing array elemts is one of the more common task that we do with an array 
	-this can be done throuh the use of array indexes
	-Each elemet in an array is associated with it's own index/number
	-In JavaScript, the first element is associated with the number 0 and increasing his number by 1 for every procedding element
	-the reason an array starts with 0 due to how the language is desgned
	-Array indexes actually refer to an address/location in the devices memory and how the information is stored
	-Example array locaion in memory
		Array[0]= 0xffe947bad0
		Array[1]= 0xffe947bad4
		Array[2]= 0xffe947bad8
*/

console.log(grades[0]);
console.log(computerBrands[0]);
// Accessig an array elemts that does not exist will return "undefied"

console.log(grades[20]);

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// We can also save/store an array items inside the variable
let currenLaker = lakersLegends[2];
console.log(currenLaker);

// you can also reassign array values using the item's indices
console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after reassignment');
console.log(lakersLegends)

// Accesiing the last elemt of an array

// Since the first element of an array with 0, subtracting 1 to the length of an array will offset the value by one allowing us to the get the last index

let bullsLegend = ["Jordan", "Pippen", "Roadman", "Rose", "Kukoc"];
// original variable = diff variable -1 (Call last array)
let lastElementIndex = bullsLegend.length-1;
console.log(bullsLegend[lastElementIndex]);

// You can also add it directly
console.log(bullsLegend[bullsLegend.length-1]);

/*I think mababawasan lang sir if inassign mo ulit sa same name ng array

let arrayName = arrayName.length-1;

Pero pag inassign mo sa ibang variable, para mo lang copy

let variableName = arrayName.length-1*/

// Adding items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = 'Cloud Strife';
console.log(newArr);

console.log(newArr[1]);
newArr[1] = 'Tifa Lockhart';
console.log(newArr);

// Adding items at the end of the array
// newArr[newArr.length-1] = "Aerith Gainsborough";
newArr[newArr.length] = "Barret Wallace";
console.log(newArr)


// Looping over an Array
// You can usea for loop to iterate over all items in an array
// Set the counter as the index and set a condition that as long the current index iterated is less than the lenth of array

for (let index= 0; index<newArr.length; index ++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];

for (let index = 0; index < numArr.length; index++){
	 if (newArr[index] % 5 === 0) {
	 	console.log(numArr[index] + " is divisible by 5");

	 } else {
	 	console.log(numArr[index] + " is divisible by 5");
	 }
	};

// [SECTION] Multidimensional Array
/*
	-Multi dimensional array are useful for storing complex data structures
	-A practical application of this is to help visualize/create real world objects
	-Though useful in a number of cases, craeting compelx array structures is not always recommended
*/

let chessBoard = [
		['a1','b1','c1','d1','e1','f1','g1','h1'],
		['a2','b2','c2','d2','e2','f2','g2','h2'],
		['a3','b3','c3','d3','e3','f3','g3','h3'],
		['a4','b4','c4','d4','e4','f4','g4','h4'],
		['a5','b5','c5','d5','e5','f5','g5','h5'],
		['a6','b6','c6','d6','e6','f6','g6','h6'],
		['a7','b7','c7','d7','e7','f7','g7','h7'],
		['a8','b8','c8','d8','e8','f8','g8','h8']
	]

console.log(chessBoard);

// accessing elements of  multidimensional array
console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);
