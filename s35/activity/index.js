const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://shiela-255:admin123@zuitt-bootcamp.ulji5nu.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


const userSchema = new mongoose.Schema({
	
	username: String,
	password: String
})



const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res)=> {
	
	
	User.findOne({username : req.body.username}).then((result, err) => {
		// If a document was found and the document's name matches the information sent via the client/Postman
		
		if(result != null && result.username == req.body.username){
			// Return a message to the client/Postman
			return res.send("Duplicate username found");

		} else {

			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			});

			
			newUser.save().then((savedUser, saveErr) => {
				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr);

				// No error found while creating the document
				} else {
					return res.status(201).send("New user registered");

				} if ( result = null ){
					return res.send (" BOTH username and password must be provided")
				}
			})
		}

	})
})

/*app.get("/", (req,res) =>{
	User.find({}).then((result,err)=>{

		if (err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})*/






// Listen to the port, meaning, if the port is accessed we run the server
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;
