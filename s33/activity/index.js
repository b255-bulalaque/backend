
// fecth request using GET method that will retrieve all the to do list items from JSON Placeholder API


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));




// create an array using the map method and return just the title of every item and print the result in the console

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map((item) => {
  return item.title
})));

/*
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    let items = data.map(item => ({
     
      title: item.title,
      
    }));
    console.log(items);
  });*/

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.Using the data retrieve, print a message in the console that will provid the title and status of the to do list item



fetch ('https://jsonplaceholder.typicode.com/todos/1', {
	
	
	method: 'GET',

	headers: {
		'Content-type': 'application/json',
	},

	
})

.then((response) => response.json())
.then((json) => console.log(json));



// Create a fetch request using POST method that will create a to do list item using JSON Placeholder API

fetch ('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Items",
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the PUT method that will update to do list items using the JSON Placeholder API

fetch ('https://jsonplaceholder.typicode.com/todos/1', {
	
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a diffrent data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));



// PATCH
fetch ('https://jsonplaceholder.typicode.com/todos/1', {
	
	
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Compete",
		title: "delectus aut autem",
		userId: 1
		
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// delete

fetch ('https://jsonplaceholder.typicode.com/todos/1', {
	
	method: 'DELETE'
});



