console.log('Hello World!');

// [SECTION] Getting all posts

/*
	-The Fetch API allows you to asynchronously request for a resource (data)

	-A "promise" is an object that represents the eventual completion(or failure) of an asynchronous function and its resulting value

*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

/*
	-REtrives all posts following the REST API

	-By using the then method we can now check for the satatus of the promise

*/

fetch('https://jsonplaceholder.typicode.com/posts')
/*
	-The 'fetch' method will return a promise that resolves to a reponse object

	-The 'then' methos captures theresponse object and returns another promse which will be eventually resolve or rejected
*/

.then(response => console.log(response.status));


// ERROR: 404 means not found
// ERROR : 500 Internal server error
// 200: success

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in oir application

.then((response) => response.json())
// Print the converted JSON value from the "fetch" request

.then((json) => console.log(json));


// The "async" and "await" keywords is another approach that an be used to achieve asychronous code
// Used in function to indicate which portion of codes should be waited for
// Create an asynchronous function
async function fetchData(){


	// waits for the "fecth" mehod to complete then stores the value in the result variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result returned by fetch returns a promise
	console.log(result);

	// the returned response is an object
	console.log(typeof result);

	// We cannot access the contemt of response by directly accessing its bodyproperty
	console.log(result.body);


	// Converts the data from the "response" object as JSON
	let json = await result.json();

	// print out the content of the "Response" object
	console.log(json);

}

fetchData();


// [SECTION] Getting a specific post

fetch ('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Creating a post


fetch ('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World',
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));


 /*aync functions and promises are good for high traffic/latency so that the code runs smoothly */

// [SECTION] Updating a post using a put method

fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	
	// Sets the method request following the REST API
	// Default method is get
	method: 'PUT',

		// Sets header data of the request object to be sent to the backend
		// Specified that the content will be in a JSON structure
	headers: {
		'Content-type': 'application/json',
	},


	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: 'Hello Again',
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));


// [SECTIOn Udapting a post using PATCH]


	/*- Updates a specific post following the REST API 
	-The difference betwwen PUT and PATCH is the number of properties being changed
	-PATCH is used to updated the whole object
	-PUT is used to update a single/several properties
*/

fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	
	
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		
		title: 'Corrected post',
		
	})
})

.then((response) => response.json())
.then((json) => console.log(json));



/*
	[SECTION] DELETEING A POST

	-Deleting a specific post following th REST API
*/

fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	
	method: 'DELETE'
});


/*
	[SECTION] FILETERING Posts

	-The data can be filetered by sending the userID along ithe URL
	-Information sent via the url can be done by adding the question mark symbol (?)

*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));


/*
	[SECTION] RETRIEVING NESTD? REALTED COOMENTS TO POSTS

	-Retrieving comments for a specidifc post follwoing the REST API


*/

fetch ('https://jsonplaceholder.typicode.com/posts/1/comments', {
	
})

.then((response) => response.json())
.then((json) => console.log(json));
